const questions = [
	{question:"What's your name?", autocomplete: "name"},
	{question:"What's your email?", pattern: /^[^\s@]+@[^\s@]+\.[^\s@]+$/, autocomplete: "email"},
	{question:"Ask me anything!", textarea: 'true'}
]

const onComplete =() => {
	const h1 = document.createElement('h1')
	h1.appendChild(document.createTextNode(`Thanks ${questions[0].answer}, I\'ll send an automated message out shortly!`))
	setTimeout(() => {
		form.parentElement.appendChild(h1)
		setTimeout(() => { h1.style.opacity = 1 }, 50)
	}, 1000)
	const xhr = new XMLHttpRequest()
	xhr.open("POST", 'https://kv.ax/contact/send', true)
	xhr.setRequestHeader('Content-Type', 'application/json')
	xhr.send(JSON.stringify({
		name:		questions[0].answer,
		email:		questions[1].answer,
		message:	questions[2].answer
	}))
}

((questions, onComplete) => {

	const tTime = 100 // transition transform time from #form in ms
	const wTime = 200 // transition width time from #form in ms
	const eTime = 1000 // transition width time from inputLabel in ms

	if (questions.length == 0) return

	let position = 0

	putQuestion()

	next.addEventListener('click', validate)
	inputField.addEventListener('keyup', ({keyCode}) => {
		transform(0, 0) // ie hack to redraw
		if (keyCode == 13) validate()
	})

	back.addEventListener('click', e => {
		if (position === 0) return
		position -= 1
		hideCurrent(putQuestion)
	})

	function putQuestion() {
		let input = document.getElementById("inputField")
		inputLabel.innerHTML = questions[position].question
		inputField.type = questions[position].type || 'text'
		inputField.value = questions[position].answer || ''
		inputField.autocomplete = questions[position].autocomplete || 'off'
		if(questions[position].textarea) {inputField.outerHTML = inputField.outerHTML.replace(/<input/, "<textarea")+"</textarea>"} 

		inputField.focus()

		// set the progress of the background
		progress.style.width = `${position * 100 / questions.length}%`

		back.className = position ? '' : 'hidden'
		next.innerHTML = position != 2 ? 'next' : 'send'
		showCurrent()

	}

	// when submitting the current question
	function validate() {

		const validateCore = () => {
			return inputField.value.match(questions[position].pattern || /.+/)
		}

		if (!questions[position].validate) questions[position].validate = validateCore

		// check if the pattern matches
		if (!questions[position].validate()) wrong(inputField.focus.bind(inputField))
		else ok(() => {

			// execute the custom end function or the default value set
			if (questions[position].done) questions[position].done()
			else questions[position].answer = inputField.value

			++position

			// if there is a new question, hide current and load next
			if (questions[position]) hideCurrent(putQuestion)
			else hideCurrent(() => {
				// remove the box if there is no next question
				form.className			=	'close'
				controls.className		=	'close'
				progress.style.width	=	'100%'

				onComplete()
			})

		})

	}

	function hideCurrent(callback) {
		inputContainer.style.opacity	=	0
		inputLabel.style.marginLeft		=	0
		inputContainer.style.border		=	null
		setTimeout(callback, wTime)
	}

	function showCurrent(callback) {
		inputContainer.style.opacity	=	1
		setTimeout(callback, wTime)
	}

	function transform(x, y) {
		form.style.transform			=	`translate(${x}px ,  ${y}px)`
	}

	function ok(callback) {
		form.className					=	''
		setTimeout(transform, tTime * 0, 0, 10)
		setTimeout(transform, tTime * 1, 0, 0)
		setTimeout(callback, tTime * 2)
	}

	function wrong(callback) {
		form.className					=	'wrong'
		for (let i = 0; i < 6; i++) // shaking motion
			setTimeout(transform, tTime * i, (i % 2 * 2 - 1) * 20, 0)
		setTimeout(transform, tTime * 6, 0, 0)
		setTimeout(callback, tTime * 7)
	}

})(questions, onComplete)